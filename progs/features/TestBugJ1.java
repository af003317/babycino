class TestBugJ1 {
  public static void main(String[] a) {
    System.out.println(new Test().f());
  }
}

class Test {
  public int f() {
    int x;
    boolean y;
    x = 0;
    y = true;
    x += y;
    return x;
  }
}
